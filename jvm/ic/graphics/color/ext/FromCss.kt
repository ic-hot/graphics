package ic.graphics.color.ext


import ic.base.throwables.UnableToParseException

import ic.graphics.color.Color
import ic.graphics.color.funs.parseColorArgbFromCssOrThrow


@Throws(UnableToParseException::class)
fun Color.Companion.fromCssOrThrowUnableToParse (cssString: String) : Color {

	return Color(parseColorArgbFromCssOrThrow(cssString))

}


fun Color.Companion.fromCss (cssString: String) : Color {
	return try {
		fromCssOrThrowUnableToParse(cssString)
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime("cssString: $cssString")
	}
}