package ic.graphics.color.funs


import ic.base.throwables.UnableToParseException
import ic.util.text.numbers.hex.parseInt32FromHexStringOrThrow

import ic.graphics.color.ColorArgb


@Throws(UnableToParseException::class)
fun parseColorArgbFromCssOrThrow (cssString: String) : ColorArgb {

	var trimmedString = cssString

	if (trimmedString.startsWith('#')) trimmedString = trimmedString.substring(1)

	if (trimmedString.length == 6) {
		trimmedString = "ff$trimmedString"
	}

	if (trimmedString.length != 8) throw UnableToParseException()

	return parseInt32FromHexStringOrThrow(trimmedString)

}


fun parseColorArgbFromCss (cssString: String) : ColorArgb {
	return try {
		parseColorArgbFromCssOrThrow(cssString)
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime("cssString: $cssString")
	}
}

fun parseColorArgbFromCssOrNull (cssString: String?) : ColorArgb? {
	if (cssString == null) return null
	try {
		return parseColorArgbFromCssOrThrow(cssString)
	} catch (t: UnableToParseException) {
		return null
	}
}