package ic.storage.res


import ic.base.throwables.NotExistsException
import ic.util.cache.RamCache

import ic.graphics.image.Image
import ic.graphics.platform.graphicsPlatform


private val cache = RamCache<String, Image>()


@Throws(NotExistsException::class)
fun getResImageOrThrow (name: String) : Image {
	return cache.getOr(name) {
		graphicsPlatform.resources.getImageOrThrow(name)
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun getResImage (name: String) : Image {
	try {
		return getResImageOrThrow(name)
	} catch (_: NotExistsException) {
		throw RuntimeException("name: $name")
	}
}