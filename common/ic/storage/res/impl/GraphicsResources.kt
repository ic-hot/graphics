package ic.storage.res.impl


import ic.base.throwables.NotExistsException

import ic.graphics.image.Image


internal interface GraphicsResources {

	@Throws(NotExistsException::class)
	fun getImageOrThrow (path: String) : Image

}