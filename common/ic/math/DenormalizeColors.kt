package ic.math


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color
import ic.graphics.color.ext.alpha
import ic.graphics.color.ext.blue
import ic.graphics.color.ext.green
import ic.graphics.color.ext.red


fun Float32.lerp (a: Color, b: Color) : Color {

	return Color(
		red 	= denormalize(a.red, 	b.red),
		green 	= denormalize(a.green, 	b.green),
		blue 	= denormalize(a.blue, 	b.blue),
		alpha 	= denormalize(a.alpha, 	b.alpha)
	)

}