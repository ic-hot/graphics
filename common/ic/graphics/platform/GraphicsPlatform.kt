package ic.graphics.platform


import ic.base.throwables.UnableToParseException
import ic.storage.res.impl.GraphicsResources
import ic.stream.sequence.ByteSequence

import ic.graphics.image.Image


internal interface GraphicsPlatform {


	val resources : GraphicsResources


	@Throws(UnableToParseException::class)
	fun parseImageFromJpegOrThrow (jpeg: ByteSequence) : Image

	@Throws(UnableToParseException::class)
	fun parseImageFromPngOrThrow (png: ByteSequence) : Image

	@Throws(UnableToParseException::class)
	fun parseImageFromGifOrThrow (gif: ByteSequence) : Image


}