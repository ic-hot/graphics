package ic.graphics.canvas


import ic.base.arrays.Int32Array
import ic.base.arrays.ext.get.get
import ic.base.arrays.ext.set
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.stream.sequence.ByteSequence

import ic.graphics.color.mix.MixMode
import ic.graphics.color.Color
import ic.graphics.color.ColorArgb
import ic.graphics.color.mix.ReplaceMixMode
import ic.graphics.image.Image


class ImageCanvas (

	override val width  : Int64,
	override val height : Int64

) : Image, Canvas {


	private val pixelsArgbArray = Int32Array(length = width * height)

	private inline fun getPixelIndexInArray (x: Int64, y: Int64) = y * width + x


	override fun drawPixel (x: Int64, y: Int64, color: Color, mixMode: MixMode) {
		if (x < 0) return
		if (x >= width) return
		if (y < 0) return
		if (y >= height) return
		val arrayIndex = getPixelIndexInArray(x, y)
		if (mixMode == ReplaceMixMode) {
			pixelsArgbArray[arrayIndex] = color.argb
		} else {
			pixelsArgbArray[arrayIndex] = mixMode.mix(
				Color(pixelsArgbArray[arrayIndex]),
				color
			).argb
		}
	}

	override fun drawPixel (x: Int64, y: Int64, color: ColorArgb, mixMode: MixMode) {
		if (x < 0) return
		if (x >= width) return
		if (y < 0) return
		if (y >= height) return
		val arrayIndex = getPixelIndexInArray(x, y)
		if (mixMode == ReplaceMixMode) {
			pixelsArgbArray[arrayIndex] = color
		} else {
			pixelsArgbArray[arrayIndex] = mixMode.mix(
				Color(pixelsArgbArray[arrayIndex]),
				Color(color)
			).argb
		}
	}


	override fun getPixelColorArgb (x: Int64, y: Int64) : ColorArgb {
		return pixelsArgbArray[getPixelIndexInArray(x, y)]
	}

	override fun getPixelColor (x: Int64, y: Int64) : Color {
		return Color(
			getPixelColorArgb(x, y)
		)
	}

	override fun toJpeg (quality: Float32): ByteSequence {
		TODO("Not yet implemented")
	}

	override fun toPng(): ByteSequence {
		TODO("Not yet implemented")
	}

	override fun copyResize (width: Int64, height: Int64): Image {
		TODO("Not yet implemented")
	}


}