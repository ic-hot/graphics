package ic.graphics.canvas


import ic.base.annotations.ToOverride
import ic.base.assert.assert
import ic.base.loop.repeat
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64

import ic.graphics.color.Color
import ic.graphics.color.mix.MixMode
import ic.graphics.color.ColorArgb
import ic.graphics.color.mix.NormalMixMode
import ic.graphics.image.Image


interface Canvas {


	val width  : Int64
	val height : Int64


	fun drawPixel (x: Int64, y: Int64, color : Color, mixMode: MixMode = NormalMixMode)

	fun drawPixel (x: Int64, y: Int64, color : ColorArgb, mixMode: MixMode = NormalMixMode)


	@ToOverride
	fun drawRect (
		x : Int64, y : Int64,
		width : Int64, height : Int64,
		color : ColorArgb, mixMode : MixMode = NormalMixMode
	) {
		repeat(height) { localY ->
			repeat(width) { localX ->
				drawPixel(
					x = x + localX,
					y = y + localY,
					color = color,
					mixMode = mixMode
				)
			}
		}
	}

	@ToOverride
	fun drawRect (
		x : Int64, y : Int64,
		width : Int64, height : Int64,
		color : Color, mixMode : MixMode = NormalMixMode
	) {
		repeat(height) { localY ->
			repeat(width) { localX ->
				drawPixel(
					x = x + localX,
					y = y + localY,
					color = color,
					mixMode = mixMode
				)
			}
		}
	}


	@ToOverride
	fun drawImage (
		x : Int64, y : Int64,
		image : Image,
		width : Int64 = image.width,
		height: Int64 = image.height,
		opacity : Float32 = 1F,
		toUseAntialiasing : Boolean = false,
		mixMode : MixMode = NormalMixMode
	) {
		assert { width == image.width }
		assert { height == image.height }
		repeat(image.height) { localY ->
			repeat(image.width) { localX ->
				drawPixel(
					x = x + localX,
					y = y + localY,
					color = image.getPixelColorArgb(localX, localY),
					mixMode = mixMode
				)
			}
		}
	}


}