package ic.graphics.canvas.upscale


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.graphics.canvas.Canvas
import ic.graphics.color.Color

import ic.graphics.color.ColorArgb
import ic.graphics.color.mix.MixMode
import ic.graphics.image.Image


abstract class UpscaledCanvas : Canvas {


	protected abstract val originalCanvas : Canvas

	protected abstract val scale : Int32


	override val width get() = originalCanvas.width / scale

	override val height get() = originalCanvas.height / scale


	override fun drawRect(
		x : Int64, y : Int64,
		width : Int64, height : Int64,
		color : ColorArgb, mixMode : MixMode
	) {
		originalCanvas.drawRect(
			x = x * scale,
			y = y * scale,
			width = width * scale,
			height = height * scale,
			color = color, mixMode = mixMode
		)
	}

	override fun drawRect(
		x : Int64, y : Int64,
		width : Int64, height : Int64,
		color : Color,
		mixMode : MixMode
	) {
		originalCanvas.drawRect(
			x = x * scale,
			y = y * scale,
			width = width * scale,
			height = height * scale,
			color = color, mixMode = mixMode
		)
	}


	override fun drawPixel (x: Int64, y: Int64, color: ColorArgb, mixMode: MixMode) {
		originalCanvas.drawRect(
			x = x * scale,
			y = y * scale,
			width = scale.asInt64,
			height = scale.asInt64,
			color = color, mixMode = mixMode
		)
	}

	override fun drawPixel (x: Int64, y: Int64, color: Color, mixMode: MixMode) {
		originalCanvas.drawRect(
			x = x * scale,
			y = y * scale,
			width = scale.asInt64,
			height = scale.asInt64,
			color = color, mixMode = mixMode
		)
	}


	override fun drawImage(
		x : Int64, y : Int64,
		image : Image,
		width : Int64, height: Int64,
		opacity : Float32,
		toUseAntialiasing : Boolean,
		mixMode : MixMode
	) {
		originalCanvas.drawImage(
			x = x * scale,
			y = y * scale,
			image = image,
			width = width * scale,
			height = height * scale,
			opacity = opacity,
			toUseAntialiasing = toUseAntialiasing,
			mixMode = mixMode
		)
	}


}