package ic.graphics.canvas.upscale


import ic.graphics.canvas.Canvas


abstract class LateInitUpscaledCanvas : UpscaledCanvas() {

	public override lateinit var originalCanvas : Canvas

}