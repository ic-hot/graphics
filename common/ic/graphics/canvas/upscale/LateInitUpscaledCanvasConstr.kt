package ic.graphics.canvas.upscale


import ic.base.primitives.int32.Int32


@Suppress("NOTHING_TO_INLINE")
inline fun LateInitUpscaledCanvas (

	scale : Int32

) : LateInitUpscaledCanvas {

	return object : LateInitUpscaledCanvas() {

		override val scale get() = scale

	}

}