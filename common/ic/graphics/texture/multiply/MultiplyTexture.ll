

Import "ic.base.primitives.float64.Float64"
Import "ic.math.funs.Multiply"

Import "ic.graphics.texture.Texture"


MultiplyTexture Texture {


	texture Texture

	multiplier Float64


	ColorRgbVector {

		thisPositionOnImage positionOnImage

		self Multiply {
			a texture.ColorRgbVector { positionOnImage thisPositionOnImage }
			b multiplier
		}

	}


}