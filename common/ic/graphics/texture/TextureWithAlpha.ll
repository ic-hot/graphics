

Import "ic.base.primitives.float32.Float32"
Import "ic.math.funs.Renormalize"


TextureWithAlpha {


	// coords from 0 to 1 -> x from left to right, y from top to bottom
	ColorRgbVector Float64Vector3 { positionOnImage Float32Vector2 }


	// coords from 0 to 1 -> x from left to right, y from top to bottom
	Alpha Float32 { positionOnImage Float32Vector2 }


	// coords from -1 to 1 -> x from left to right, y from top to bottom
	ColorRgbVectorByCenteredPosition { centeredPositionOnImage Float32Vector2
		self ColorRgbVector {
			positionOnImage Renormalize {
				value centeredPositionOnImage
				oldStart -1
				oldEnd 1
				newStart 0
				newEnd 1
			}
		}
	}


}