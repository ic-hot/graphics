package ic.graphics.color


import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32


typealias ColorArgb = Int32


inline fun ColorArgb (value: Int64) : ColorArgb = value.asInt32