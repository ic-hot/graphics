package ic.graphics.color


import kotlin.jvm.JvmInline


@JvmInline
value class Color (

	val argb : ColorArgb

) {

	companion object {

		val Transparent	         = Color(0.0, 0.0, 0.0, 0.0 )
		val TransparentBlack     = Color(0.0, 0.0, 0.0, 0.5 )
		val TransparentWhite     = Color(1.0, 1.0, 1.0, 0.5 )
		val MoreTransparentBlack = Color(0.0, 0.0, 0.0, 0.25)
		val LessTransparentBlack = Color(0.0, 0.0, 0.0, 0.75)
		val TransparentGray      = Color(0.5, 0.5, 0.5, 0.5 )

		val Black = Color(0.0, 0.0, 0.0)
		val White = Color(1.0, 1.0, 1.0)
		val Gray  = Color(0.5, 0.5, 0.5)

		val DarkerGray	    = Color( 0.125,  0.125,  0.125  )
		val DarkGray	    = Color( 0.25,   0.25,   0.25   )
		val LightGray	    = Color( 0.75,   0.75,   0.75   )
		val LighterGray	    = Color( 0.875,  0.875,  0.875  )
		val EvenLighterGray	= Color( 0.9375, 0.9375, 0.9375 )

		val Red	  = Color(1.0, 0.0, 0.0)
		val Green = Color(0.0, 1.0, 0.0)
		val Blue  = Color(0.0, 0.0, 1.0)

		val Yellow  = Color(1.0, 1.0, 0.0)
		val Cyan    = Color(0.0, 1.0, 1.0)
		val Magenta = Color(1.0, 0.0, 1.0)

		val Orange = Color(1.0, 0.5, 0.0)

		val Pink = Color(1.0, 0.5, 0.5)

		val SkyBlue = Color(0.0, 0.5, 1.0)
		val CalmGreen = Color(0.25, 0.75, 0.25)

		val LightCalmGreen = Color(0.5, 0.875, 0.5)
		val LightSkyBlue   = Color(0.5, 0.75, 1.0)

		val LighterSkyBlue   = Color(0.75, 0.875, 1.0)

	}

}