package ic.graphics.color.util


import ic.base.primitives.float32.Float32
import ic.math.normalize

import ic.graphics.color.Color
import ic.math.lerp


fun Float32.interpolateLinearly (vararg points: Pair<Float32, Color>) : Color {

	var i = 0
	while (i < points.size) {
		if (this >= points[i].first) {
			if (i == points.size - 1) {
				return points[i].second
			} else {
				if (this < points[i + 1].first) {
					normalize(points[i].first, points[i + 1].first).lerp(points[i].second, points[i + 1].second)
				}
			}
		}
		i++
	}; return points[0].second

}