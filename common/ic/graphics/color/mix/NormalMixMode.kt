package ic.graphics.color.mix


import ic.graphics.color.Color
import ic.graphics.color.ext.alpha
import ic.graphics.color.ext.blue
import ic.graphics.color.ext.green
import ic.graphics.color.ext.red


object NormalMixMode : MixMode {


	override fun mix (dst: Color, src: Color) : Color {

		val f = src.alpha

		return Color(

			red   = dst.red   * (1 - f) + src.red   * f,
			green = dst.green * (1 - f) + src.green * f,
			blue  = dst.blue  * (1 - f) + src.blue  * f,

			alpha = 1 - ((1 - dst.alpha) * (1 - src.alpha))

		)

	}


}