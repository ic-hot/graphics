package ic.graphics.color.mix


import ic.graphics.color.Color


fun mix (dst: Color, src: Color, mode: MixMode = NormalMixMode) : Color {

	return mode.mix(dst, src)

}