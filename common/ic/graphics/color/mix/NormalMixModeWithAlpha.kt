package ic.graphics.color.mix


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64

import ic.graphics.color.Color
import ic.graphics.color.ext.alpha
import ic.graphics.color.ext.blue
import ic.graphics.color.ext.green
import ic.graphics.color.ext.red


class NormalMixModeWithAlpha (private val alpha : Float64) : MixMode {


	constructor (alpha: Float32) : this (alpha.asFloat64)


	override fun mix (dst: Color, src: Color) : Color {

		val srcAlpha = src.alpha * alpha

		val srcMixFactor = 1 - ((1 - srcAlpha) * dst.alpha)

		return Color(

			red = dst.red * (1 - srcMixFactor) + src.red * srcMixFactor,

			green = dst.green * (1 - srcMixFactor) + src.green * srcMixFactor,

			blue = dst.blue * (1 - srcMixFactor) + src.blue * srcMixFactor,

			alpha = 1 - ((1 - dst.alpha) * (1 - srcAlpha))

		)

	}


}