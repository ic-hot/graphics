package ic.graphics.color.mix


import ic.graphics.color.Color


interface MixMode {


	fun mix (dst : Color, src : Color) : Color


}