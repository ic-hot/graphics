package ic.graphics.color.impl


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32


internal fun float32ToByte (f: Float32) : Int32 {

	var b = (f * 255).asInt32

	if (b < 0) b = 0 else if (b > 255) b = 255

	return b

}