package ic.graphics.color.impl


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32


@Suppress("NOTHING_TO_INLINE")
internal inline fun byteToFloat32 (byte: Int32) : Float32 = byte / 255F