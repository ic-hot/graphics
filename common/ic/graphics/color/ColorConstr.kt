package ic.graphics.color


import ic.base.primitives.bytes.ext.asInt32
import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.base.primitives.int32.ext.asByte
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32

import ic.graphics.color.impl.float32ToByte


fun Color (red: Float32, green: Float32, blue: Float32, alpha: Float32 = 1F) : Color {
	return Color(
		argb = (
			(float32ToByte(alpha) shl 8 * 3) or
			(float32ToByte(red)   shl 8 * 2) or
			(float32ToByte(green) shl 8 * 1) or
			(float32ToByte(blue)  shl 8 * 0)
		)
	)
}

@Suppress("NOTHING_TO_INLINE")
inline fun Color (red: Float64, green: Float64, blue: Float64, alpha: Float64 = 1.0) : Color {
	return Color(
		red   = red   .asFloat32,
		green = green .asFloat32,
		blue  = blue  .asFloat32,
		alpha = alpha .asFloat32
	)
}

@Suppress("NOTHING_TO_INLINE")
inline fun Color (red: Byte, green: Byte, blue: Byte, alpha: Byte = 255.asByte) : Color {
	return Color(
		argb = (
			(alpha .asInt32 shl 8 * 3) or
			(red   .asInt32 shl 8 * 2) or
			(green .asInt32 shl 8 * 1) or
			(blue  .asInt32 shl 8 * 0)
		)
	)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Color (argb: Int64) = Color(argb = argb.asInt32)