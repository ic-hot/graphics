package ic.graphics.color.ext


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color


fun Color.copy (
	red   : Float32 = this.red,
	green : Float32 = this.green,
	blue  : Float32 = this.blue,
	alpha : Float32 = this.alpha,
) : Color {
	return Color(
		red   = red,
		green = green,
		blue  = blue,
		alpha = alpha
	)
}