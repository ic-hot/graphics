package ic.graphics.color.ext


import ic.graphics.color.Color
import ic.graphics.color.impl.float32ToByte


val Color.asCss : String get() = (

	"rgba(" +
		float32ToByte(red) + "," +
		float32ToByte(green) + "," +
		float32ToByte(blue) + "," +
		alpha +
	")"

)