package ic.graphics.color.ext


import ic.base.strings.ext.startsWith
import ic.base.strings.inUpperCase
import ic.base.throwables.UnableToParseException
import ic.util.text.numbers.hex.parseInt32FromHexStringOrThrow

import ic.graphics.color.Color


@Throws(UnableToParseException::class)
fun Color.Companion.parseFromCssOrThrow (cssString: String) : Color {
	var trimmedString = cssString.inUpperCase
	if (trimmedString startsWith '#') {
		trimmedString = trimmedString.substring(1)
	}
	if (trimmedString.length == 6) {
		trimmedString = "FF$trimmedString"
	}
	if (trimmedString.length != 8) throw UnableToParseException()
	return Color(
		argb = parseInt32FromHexStringOrThrow(trimmedString)
	)
}


fun Color.Companion.parseFromCssOrNull (cssString: String) : Color? {
	try {
		return parseFromCssOrThrow(cssString)
	} catch (_: UnableToParseException) {
		return null
	}
}