package ic.graphics.color.ext


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color


fun Color.fade (opacity: Float32) = Color(
	red = red,
	green = green,
	blue = blue,
	alpha = alpha * opacity
)