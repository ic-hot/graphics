package ic.graphics.color.ext


import ic.graphics.color.Color
import ic.graphics.color.impl.byteToFloat32


val Color.alpha get() = byteToFloat32(argb ushr 8 * 3 and 0x000000FF)

val Color.red get() = byteToFloat32(argb ushr 8 * 2 and 0x000000FF)

val Color.green get() = byteToFloat32(argb ushr 8 * 1 and 0x000000FF)

val Color.blue get() = byteToFloat32(argb ushr 8 * 0 and 0x000000FF)
