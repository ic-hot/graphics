package ic.graphics.image


import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.stream.sequence.ByteSequence

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb


interface Image {

	val width  : Int64
	val height : Int64

	fun getPixelColorArgb (x: Int64, y: Int64) : ColorArgb

	fun getPixelColor (x: Int64, y: Int64) : Color {
		return Color(
			getPixelColorArgb(x, y)
		)
	}

	fun toJpeg (quality: Float32 = .75F) : ByteSequence

	fun toPng() : ByteSequence

	fun copyResize (width: Int64, height: Int64) : Image

	companion object

}