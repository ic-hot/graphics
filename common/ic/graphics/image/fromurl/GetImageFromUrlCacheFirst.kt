package ic.graphics.image.fromurl


import ic.base.throwables.IoException
import ic.base.throwables.UnableToParseException
import ic.network.http.HttpException

import ic.graphics.image.Image
import ic.graphics.image.ext.copyResize
import ic.graphics.image.fromurl.impl.*
import ic.graphics.image.fromurl.impl.getImageFileFromServer
import ic.graphics.util.ResizeMode


@Throws(IoException::class, HttpException::class, UnableToParseException::class)
fun getImageFromUrlCacheFirst (

	urlString : String,

	resizeMode : ResizeMode = ResizeMode.LeaveAsIs,

) : Image {

	return imagesFromUrlRamCache.getOr(
		UrlAndResizeMode(
			urlString = urlString,
			resizeMode = resizeMode
		)
	) {
		parseImageOrThrowUnableToParse(
			getImageFileFromServer(urlString),
			urlString = urlString
		).copyResize(resizeMode)
	}

}