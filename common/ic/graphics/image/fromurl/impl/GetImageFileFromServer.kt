package ic.graphics.image.fromurl.impl


import ic.base.throwables.IoException
import ic.base.throwables.ext.stackTraceAsString
import ic.network.http.HttpException
import ic.network.http.sendHttpRequest
import ic.stream.sequence.ByteSequence
import ic.util.log.LogLevel
import ic.util.log.logD


@Throws(IoException::class, HttpException::class)
fun getImageFileFromServer (urlString : String) : ByteSequence {

	try {

		val response = sendHttpRequest(
			urlString = urlString,
			toIgnoreTrustCertificate = true,
			successLogLevel = LogLevel.None
		)

		logD("getImageFileFromServer") { "$urlString success" }

		return response.body

	} catch (e: Exception) {

		logD("getImageFileFromServer") { "$urlString exception ${ e.stackTraceAsString }" }

		throw e

	}

}