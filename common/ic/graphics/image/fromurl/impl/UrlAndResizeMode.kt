package ic.graphics.image.fromurl.impl


import ic.graphics.util.ResizeMode


data class UrlAndResizeMode (

	val urlString : String,

	val resizeMode : ResizeMode

)