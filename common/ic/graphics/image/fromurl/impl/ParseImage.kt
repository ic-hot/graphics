package ic.graphics.image.fromurl.impl


import ic.base.strings.ext.endsWith
import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ByteSequence
import ic.util.log.logW

import ic.graphics.image.Image
import ic.graphics.image.ext.fromGifOrThrowUnableToParse
import ic.graphics.image.ext.fromPngOrThrow


@Throws(UnableToParseException::class)
fun parseImageOrThrowUnableToParse (

	imageFile : ByteSequence,

	urlString : String?

) : Image {

	return when {

		urlString.endsWith(".gif") -> {
			try {
				Image.fromGifOrThrowUnableToParse(imageFile)
			} catch (t: UnableToParseException) {
				logW("Image.fromUrl") { "Unable to parse. url: $urlString" }
				throw t
			}
		}

		else -> {
			try {
				Image.fromPngOrThrow(imageFile)
			} catch (t: UnableToParseException) {
				logW("Image.fromUrl") { "Unable to parse. url: $urlString" }
				throw t
			}
		}

	}

}