package ic.graphics.image.fromurl.impl


import ic.util.cache.RamCache

import ic.graphics.image.Image


internal val imagesFromUrlRamCache = RamCache<UrlAndResizeMode, Image>()