package ic.graphics.image.fromurl


import ic.base.throwables.IoException
import ic.base.throwables.NotExistsException
import ic.base.throwables.UnableToParseException
import ic.design.task.Task
import ic.design.task.scope.ext.doInUiThread
import ic.network.http.response.HttpResponse
import ic.network.http.HttpException
import ic.parallel.funs.doInBackgroundAsTask

import ic.graphics.image.Image
import ic.graphics.image.ext.copyResize
import ic.graphics.image.fromurl.impl.*
import ic.graphics.util.ResizeMode


@Deprecated("Use blocking function")
fun loadImageFromUrlCacheFirst (

	urlString : String,

	resizeMode : ResizeMode = ResizeMode.LeaveAsIs,

	onFinish : () -> Unit = {},

	onConnectionFailure : () -> Unit,
	onHttpError : (HttpResponse) -> Unit,
	onUnableToParse : () -> Unit,

	onSuccess : (Image) -> Unit

) : Task? {

	try {

		val image = imagesFromUrlRamCache.getOrThrow(
			UrlAndResizeMode(
				urlString = urlString,
				resizeMode = resizeMode
			)
		)
		onFinish()
		onSuccess(image)
		return null

	} catch (_: NotExistsException) {

		return doInBackgroundAsTask {

			try {

				val image = parseImageOrThrowUnableToParse(
					imageFile = getImageFileFromServer(urlString),
					urlString = urlString
				).copyResize(resizeMode)

				doInUiThread {
					onFinish()
					onSuccess(image)
				}

			} catch (_: IoException) {

				doInUiThread {
					onFinish()
					onConnectionFailure()
				}

			} catch (e: HttpException) {

				doInUiThread {
					onFinish()
					onHttpError(e.response)
				}

			} catch (_: UnableToParseException) {

				doInUiThread {
					onFinish()
					onUnableToParse()
				}

			}

		}

	}

}