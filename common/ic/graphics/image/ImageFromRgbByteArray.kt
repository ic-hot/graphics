package ic.graphics.image


import ic.base.arrays.bytes.ext.get
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.stream.sequence.ByteSequence

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb


class ImageFromRgbByteArray (

	override val width 	: Int64,
	override val height : Int64,

	private val rgbByteArray: ByteArray

) : Image {

	override fun getPixelColor (x: Int64, y: Int64) : Color {
		val pixelPositionInArray = (y * width + x) * 3
		return Color(
			rgbByteArray[pixelPositionInArray],
			rgbByteArray[pixelPositionInArray + 1],
			rgbByteArray[pixelPositionInArray + 2]
		)
	}

	override fun toJpeg (quality: Float32): ByteSequence {
		TODO("Not yet implemented")
	}

	override fun toPng(): ByteSequence {
		TODO("Not yet implemented")
	}

	override fun copyResize(width: Int64, height: Int64): Image {
		TODO("Not yet implemented")
	}


	override fun getPixelColorArgb (x: Int64, y: Int64) : ColorArgb {
		return getPixelColor(x, y).argb
	}

}