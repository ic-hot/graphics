

Render {

	imageSize Int32Vector2

	passesCount Int32

	// pixelCoords: from [0, 0] to imageSize
	PixelColorVector Float64Vector3 { pixelCoords Int32Vector2 }

}