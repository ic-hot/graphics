#ifndef ic_graphics_image_render_funs_OpenRender_c_included
#define ic_graphics_image_render_funs_OpenRender_c_included


#include "../../../../base/objects/Ref.h"
#include "../../../../base/primitives/void/Void.h"

#include "../Render.h"


Void openRender (Render* render) {

	Void (*openFunction)(Ref) = (*render).openFunction;

	(*openFunction)(render);

}


#endif
