#ifndef ic_graphics_image_render_funs_Render_c_included
#define ic_graphics_image_render_funs_Render_c_included


#include "../../../../base/objects/Ref.h"
#include "../../../../base/primitives/void/Void.h"

#include "../Render.h"
#include "../RenderInput.h"


Void render (Render* render, RenderInput input) {

    Void (*renderFunction)(Ref, RenderInput) = (*render).renderFunction;

    (*renderFunction)(render, input);

}


#endif
