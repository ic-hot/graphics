#ifndef ic_graphics_image_render_funs_CloseRender_c_included
#define ic_graphics_image_render_funs_CloseRender_c_included


#include "../../../../base/objects/Ref.h"
#include "../../../../base/primitives/void/Void.h"

#include "../Render.h"


Void closeRender (Render* render) {

	Void (*closeFunction)(Ref) = (*render).closeFunction;

	(*closeFunction)(render);

}


#endif
