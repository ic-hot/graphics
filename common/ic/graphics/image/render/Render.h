#ifndef ic_graphics_image_render_Render_h_included
#define ic_graphics_image_render_Render_h_included


#include "../../../base/objects/Ref.h"
#include "../../../base/primitives/void/Void.h"

#include "RenderInput.h"


struct Render {

	Ref stateRef;

	Void (*openFunction)(Ref);

	Void (*renderFunction)(Ref, RenderInput);

	Void (*closeFunction)(Ref);

};

typedef struct Render Render;


#endif
