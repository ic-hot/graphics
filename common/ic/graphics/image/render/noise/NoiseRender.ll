Import "ic.graphics.image.render.Render"


NoiseRender Render {

	PixelColorVector {
		self Float64Vector3 {
			x Float64 InInterval { from 0, to 1 }
			y Float64 InInterval { from 0, to 1 }
			z Float64 InInterval { from 0, to 1 }
		}
	}

}