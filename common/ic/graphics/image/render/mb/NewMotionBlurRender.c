#ifndef ic_graphics_image_render_mb_MotionBlurRender_c_included
#define ic_graphics_image_render_mb_MotionBlurRender_c_included


#include <stdlib.h>

#include "../Render.h"
#include "MotionBlurRenderConfig.h"
#include "impl/Close.c"
#include "impl/MotionBlurRenderState.h"
#include "impl/Open.c"
#include "impl/Render.c"


Render newMotionBlurRender (MotionBlurRenderConfig config) {

	Render render;

	render.openFunction 	= &MotionBlurRender_open;
	render.renderFunction 	= &MotionBlurRender_render;
	render.closeFunction 	= &MotionBlurRender_close;

	MotionBlurRenderState* stateRef = malloc(sizeof(MotionBlurRenderState));

	(*stateRef).config = config;

    render.stateRef = stateRef;

	return render;

}


#endif
