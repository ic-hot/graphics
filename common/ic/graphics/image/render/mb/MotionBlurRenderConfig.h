#ifndef ic_graphics_image_render_mb_MotionBlurRenderConfig_h_included
#define ic_graphics_image_render_mb_MotionBlurRenderConfig_h_included


#include "../../../../base/primitives/int32/Int32.h"
#include "../../../../base/vectors/float64vector3/Float64Vector3.h"
#include "../../../../base/vectors/int32vector2/Int32Vector2.h"


struct MotionBlurRenderConfig {

	Int32Vector2 imageSize;

	Int32 maxTimeToFullyRedrawMs;

	Int32 maxFramesCount;

	Float64Vector3 (*getPixelColorFloat64Vector3Function)(Int32Vector2, Ref);

	Ref argRef;

};

typedef struct MotionBlurRenderConfig MotionBlurRenderConfig;


#endif

