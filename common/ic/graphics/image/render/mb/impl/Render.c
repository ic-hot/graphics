#ifndef ic_graphics_image_render_mb_impl_Render_c_included
#define ic_graphics_image_render_mb_impl_Render_c_included


#include "../../../../../base/primitives/bool/Boolean.h"
#include "../../../../../base/primitives/int32/Int32.h"
#include "../../../../../base/primitives/status/Status.h"
#include "../../../../../base/primitives/void/Void.h"

#include "../../Render.h"
#include "../../RenderInput.h"
#include "RenderFrame.c"


Void MotionBlurRender_render (Render* renderRef, RenderInput input) {

	MotionBlurRenderState* stateRef = (*renderRef).stateRef;

	Int32 maxFramesCount = (*stateRef).config.maxFramesCount;

	Int32 currentFrame = 0;

	while (
		*(input.statusRef) == STATUS_OK
	) {

		if (currentFrame >= maxFramesCount) {
			currentFrame = 0;
		}

		MotionBlurRender_renderFrame(renderRef, input, currentFrame);

		currentFrame++;

	}

}


#endif
