#ifndef ic_graphics_image_render_mb_impl_Close_c_included
#define ic_graphics_image_render_mb_impl_Close_c_included


#include <stdlib.h>

#include "../../../../../base/primitives/int32/Int32.h"
#include "../../../../../base/primitives/void/Void.h"

#include "../../Render.h"
#include "../MotionBlurRenderConfig.h"
#include "MotionBlurRenderState.h"


Void MotionBlurRender_close (Render* renderRef) {

	MotionBlurRenderState* stateRef = (*renderRef).stateRef;

	MotionBlurRenderConfig config = (*stateRef).config;

	Int32 maxFramesCount = config.maxFramesCount;

	free(
		(*stateRef).times
	);

	for (Int32 i = 0; i < maxFramesCount; i++) {
    	free(
    		*((*stateRef).pixelArrays + i)
    	);
    }

    free(
    	(*stateRef).pixelArrays
    );

    free(
        (*stateRef).masterPixelArray
    );

    free(
        (*stateRef).image.rgbaPixelsArray
    );

	free(stateRef);

}


#endif
