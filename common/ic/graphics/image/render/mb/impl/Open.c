#ifndef ic_graphics_image_render_mb_impl_Open_c_included
#define ic_graphics_image_render_mb_impl_Open_c_included


#include <stdlib.h>

#include "../../../../../base/objects/Ref.h"
#include "../../../../../base/primitives/int32/Int32.h"
#include "../../../../../base/primitives/int64/Int64.h"
#include "../../../../../base/primitives/void/Void.h"
#include "../../../../../base/vectors/int32vector2/Int32Vector2.h"
#include "../../../../../base/vectors/float64vector3/Float64Vector3.h"

#include "../../Render.h"
#include "../MotionBlurRenderConfig.h"
#include "MotionBlurRenderState.h"


Void MotionBlurRender_open (Render* renderRef) {

	MotionBlurRenderState* stateRef = (*renderRef).stateRef;

	MotionBlurRenderConfig config = (*stateRef).config;

	Int32 pixelsCount = config.imageSize.x * config.imageSize.y;

	Int32 maxFramesCount = config.maxFramesCount;

	(*stateRef).times = malloc(sizeof(Int64) * maxFramesCount);

	(*stateRef).pixelArrays = malloc(sizeof(Ref) * maxFramesCount);

	for (Int32 i = 0; i < maxFramesCount; i++) {
		(*stateRef).pixelArrays[i] = malloc(sizeof(Float64Vector3) * pixelsCount);
	}

	(*stateRef).masterPixelArray = malloc(sizeof(Float64Vector3) * pixelsCount);

	(*stateRef).image.size = config.imageSize;
	(*stateRef).image.rgbaPixelsArray = malloc(sizeof(Int32) * pixelsCount);

}


#endif
