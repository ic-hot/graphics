#ifndef ic_graphics_image_render_mb_impl_RenderFrame_c_included
#define ic_graphics_image_render_mb_impl_RenderFrame_c_included


#include "../../../../../base/primitives/int32/Int32.h"
#include "../../../../../base/primitives/void/Void.h"
#include "../../../../../base/vectors/float64vector3/Float64Vector3.h"
#include "../../../../../base/vectors/int32vector2/NewInt32Vector2.c"

#include "../../Render.h"
#include "../../RenderInput.h"
#include "../MotionBlurRenderConfig.h"
#include "MakeImage.c"
#include "MotionBlurRenderState.h"


Void MotionBlurRender_renderFrame (Render* renderRef, RenderInput input, Int32 currentFrame) {

	MotionBlurRenderState* stateRef = (*renderRef).stateRef;

	MotionBlurRenderConfig config = (*stateRef).config;

	Float64Vector3* pixelArray = (*stateRef).pixelArrays[currentFrame];

	Float64Vector3 (*getPixelColorFloat64Vector3Function)(Int32Vector2, Ref) = config.getPixelColorFloat64Vector3Function;

	for (Int32 y = 0; y < config.imageSize.y; y++) for (Int32 x = 0; x < config.imageSize.x; x++) {

		pixelArray[y * config.imageSize.x + x] = (*getPixelColorFloat64Vector3Function)(
			newInt32Vector2(x, y),
			config.argRef
		);

	}

	MotionBlurRender_makeImage(renderRef, input, currentFrame);

}


#endif
