#ifndef ic_graphics_image_render_mb_impl_MotionBlurRenderState_h_included
#define ic_graphics_image_render_mb_impl_MotionBlurRenderState_h_included


#include "../../../../../base/primitives/int64/Int64.h"
#include "../../../../../base/vectors/float64vector3/Float64Vector3.h"

#include "../../../Image.h"
#include "../MotionBlurRenderConfig.h"


struct MotionBlurRenderState {

	MotionBlurRenderConfig config;

	Int64* times;

	Float64Vector3** pixelArrays;

	Float64Vector3* masterPixelArray;

	Image image;

};

typedef struct MotionBlurRenderState MotionBlurRenderState;


#endif

