#ifndef ic_graphics_image_render_mb_impl_MakeImage_c_included
#define ic_graphics_image_render_mb_impl_MakeImage_c_included


#include "../../../../../base/primitives/int32/Int32.h"
#include "../../../../../base/primitives/void/Void.h"
#include "../../../../../base/vectors/float64vector3/Float64Vector3.h"
#include "../../../../../base/vectors/float64vector3/NewFloat64Vector3.c"
#include "../../../../../base/vectors/float64vector3/funs/DivideFloat64Vector3ByInt32.c"
#include "../../../../../base/vectors/float64vector3/funs/Float64Vector3Sum2.c"
#include "../../../../../system/funs/GetUptime.c"

#include "../../Render.h"
#include "../../RenderInput.h"
#include "../MotionBlurRenderConfig.h"
#include "MotionBlurRenderState.h"


Void MotionBlurRender_makeImage (Render* renderRef, RenderInput input, Int32 currentFrame) {

	MotionBlurRenderState* stateRef = (*renderRef).stateRef;

	MotionBlurRenderConfig config = (*stateRef).config;

	Int32 pixelsCount = config.imageSize.x * config.imageSize.y;

	Float64Vector3* masterPixelArray = (*stateRef).masterPixelArray;
	for (Int32 i = 0; i < pixelsCount; i++) {
    	masterPixelArray[i] = newFloat64Vector3(0, 0, 0);
    }

	Int64 currentTimeMs = getUptimeMs();

	Float64* times = (*stateRef).times;

	times[currentFrame] = currentTimeMs;

	Int32 frameIndexToProcess = currentFrame;
	Int32 framesProcessed = 0;

	while (framesProcessed < config.maxFramesCount) {

		if (currentTimeMs - times[frameIndexToProcess] > config.maxTimeToFullyRedrawMs) break;

		Float64Vector3* pixelArray = (*stateRef).pixelArrays[frameIndexToProcess];

		for (Int32 i = 0; i < pixelsCount; i++) {
			masterPixelArray[i] = float64Vector3Sum2(
				masterPixelArray[i],
				pixelArray[i]
			);
		}

		frameIndexToProcess--; if (frameIndexToProcess < 0) frameIndexToProcess = config.maxFramesCount - 1;

		framesProcessed++;

	}

	for (Int32 y = 0; y < config.imageSize.y; y++) for (Int32 x = 0; x < config.imageSize.x; x++) {

		setImagePixelColorFloat64Vector3(
			(*stateRef).image,
			newInt32Vector2(x, y),
			divideFloat64Vector3ByInt32(
				masterPixelArray[y * config.imageSize.x + x],
				framesProcessed
			)
		);

	}

	Void (*onProgressFunction)(Image, Ref, Status*) = input.onProgressFunction;

	(*onProgressFunction)(
		(*stateRef).image,
		input.callbackArg,
		input.statusRef
	);

}


#endif
