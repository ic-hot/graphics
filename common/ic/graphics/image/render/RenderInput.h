#ifndef ic_graphics_image_render_RenderInput_h_included
#define ic_graphics_image_render_RenderInput_h_included


#include "../../../base/objects/Ref.h"
#include "../../../base/primitives/status/Status.h"
#include "../../../base/primitives/void/Void.h"

#include "../Image.h"


struct RenderInput {

	Void (*onProgressFunction)(Image, Ref, Status*);

	Void (*onDoneFunction)(Image, Ref, Status*);

	Ref callbackArg;

	Status* statusRef;

};

typedef struct RenderInput RenderInput;


#endif
