Import "ic.system.CpuCoresCount"

Import "ic.graphics.image.Image"
Import "ic.graphics.image.render.Render"
Import "ic.graphics.image.frompixelsfloat64vector3array.ImageFromPixelsFloat64Vector3Array"


RenderImage Image {

	render Render

	ProgressCallback {
		isEnabled Boolean
		OnProgress Nothing { progress Float32, image Image }
		OnSuccess Nothing { image Image }
    }

	progressCallback ProgressCallback

	imageSize render.imageSize

	pixelsCount ( * imageSize.x imageSize.y )

	passesCount render.passesCount

	pixelsVectorsArray MutableArray {
		Item Float64Vector3
		length pixelsCount
		InitItem {
			self Float64Vector3 { x 0, y 0, z 0 }
		}
	}

	do Repeat { count passesCount, Action { passIndex index

		do RepeatInBeam {
			count pixelsCount, maxThreadsCount cpuCoresCount,
			Action { pixelIndex index

				pixelColor render.PixelColorVector {
			 		pixelCoords Int32Vector2 {
                		x ( % pixelIndex imageSize.x )
                		y ( / pixelIndex imageSize.x )
                	}
				}

				oldPixelColor pixelsVectorsArray.ItemByIndex { index pixelIndex }
				newPixelColor ( + oldPixelColor pixelColor )
				do pixelsVectorsArray.Set { index pixelIndex, value newPixelColor }

			}
		}

		completedPassesCount ( +1 passIndex )

		isLastPass ( = completedPassesCount passesCount )

		void If { condition isLastPass, IfFalse {

			void If { condition progressCallback.isEnabled, IfTrue {

				do progressCallback.OnProgress {
					progress ( / (Float32 completedPassesCount) passesCount )
					image ImageFromPixelsFloat64Vector3Array {
                    	size imageSize
                    	array pixelsVectorsArray
                    	divideBy completedPassesCount
                    }
				}

			} }

		} }

	} }

	result Image ImageFromPixelsFloat64Vector3Array {
    	size imageSize
    	array pixelsVectorsArray
    	divideBy passesCount
    }

    do progressCallback.OnSuccess { image result }

	self result

}