#ifndef ic_graphics_image_Image_h_included
#define ic_graphics_image_Image_h_included


#include "../../base/objects/Ref.h"
#include "../../base/vectors/int32vector2/Int32Vector2.h"


struct Image {
	Int32Vector2 size;
	Ref rgbaPixelsArray;
};

typedef struct Image Image;


#endif
