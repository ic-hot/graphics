package ic.graphics.image


import ic.base.arrays.Int32Array
import ic.base.arrays.ext.get.get
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.stream.sequence.ByteSequence

import ic.graphics.color.ColorArgb


class ImageFromArgbPixelsArray (

	override val width 	: Int64,
	override val height : Int64,

	private val argbPixelsArray : Int32Array

) : Image {


	override fun getPixelColorArgb (x: Int64, y: Int64) : ColorArgb {
		return argbPixelsArray[
			y * width + x
		]
	}

	override fun toJpeg (quality: Float32) : ByteSequence {
		TODO("Not yet implemented")
	}

	override fun toPng(): ByteSequence {
		TODO("Not yet implemented")
	}

	override fun copyResize(width: Int64, height: Int64): Image {
		TODO("Not yet implemented")
	}


}