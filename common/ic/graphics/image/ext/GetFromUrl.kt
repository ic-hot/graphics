@file:Suppress("NOTHING_TO_INLINE")


package ic.graphics.image.ext


import ic.base.throwables.IoException
import ic.base.throwables.UnableToParseException
import ic.base.throwables.ext.stackTraceAsString
import ic.network.http.HttpException
import ic.util.log.logW

import ic.graphics.image.Image
import ic.graphics.image.fromurl.getImageFromUrlCacheFirst
import ic.graphics.util.ResizeMode


@Throws(IoException::class, HttpException::class, UnableToParseException::class)
inline fun Image.Companion.getFromUrlCacheFirst (
	urlString : String,
	resizeMode : ResizeMode = ResizeMode.LeaveAsIs
) : Image {
	return getImageFromUrlCacheFirst(urlString = urlString, resizeMode = resizeMode)
}


inline fun Image.Companion.getFromUrlCacheFirstOrNull (
	urlString : String?,
	resizeMode : ResizeMode = ResizeMode.LeaveAsIs
) : Image? {
	if (urlString == null) return null
	try {
		return getImageFromUrlCacheFirst(
			urlString = urlString,
			resizeMode = resizeMode
		)
	} catch (e: Exception) {
		logW("Uncaught") {
			"Image.getFromUrlCacheFirstOrNull $urlString exception ${ e.stackTraceAsString }"
		}
		return null
	}
}