package ic.graphics.image.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.stream.sequence.ByteSequence

import ic.graphics.color.ColorArgb
import ic.graphics.image.Image


fun Image.copyUpscale (scalar : Int32) : Image {

	val copy = copy()

	return object : Image {

		override val width  get() = copy.width  * scalar
		override val height get() = copy.height * scalar

		override fun getPixelColorArgb (x: Int64, y: Int64) : ColorArgb {
			return copy.getPixelColorArgb(
				x / scalar,
				y / scalar
			)
		}

		override fun toJpeg (quality: Float32): ByteSequence {
			TODO("Not yet implemented")
		}

		override fun toPng() : ByteSequence {
			TODO("Not yet implemented")
		}

		override fun copyResize (width: Int64, height: Int64) : Image {
			TODO("Not yet implemented")
		}

	}

}