package ic.graphics.image.ext


import ic.base.throwables.WrongValueException

import ic.graphics.image.Image
import ic.graphics.util.ResizeMode


fun Image.copyResize (resizeMode: ResizeMode) : Image {

	when (resizeMode) {

		is ResizeMode.LeaveAsIs -> return this

		is ResizeMode.MaxWidth -> {

			if (width <= resizeMode.maxWidth) return this

			return copyResize(
				resizeMode.maxWidth,
				height * resizeMode.maxWidth / width
			)

		}

		is ResizeMode.MaxHeight -> {

			if (height <= resizeMode.maxHeight) return this

			return copyResize(
				width * resizeMode.maxHeight / height,
				resizeMode.maxHeight
			)

		}

		is ResizeMode.MaxDimension -> {

			if (width >= height) {

				if (width <= resizeMode.maxDimension) return this

				return copyResize(
					resizeMode.maxDimension,
					height * resizeMode.maxDimension / width
				)

			} else {

				if (height <= resizeMode.maxDimension) return this

				return copyResize(
					width * resizeMode.maxDimension / height,
					resizeMode.maxDimension
				)

			}

		}

		else -> throw WrongValueException.Runtime("resizeMode: $resizeMode")

	}

}