package ic.graphics.image.ext


import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ByteSequence

import ic.graphics.image.Image
import ic.graphics.platform.graphicsPlatform


@Throws(UnableToParseException::class)
fun Image.Companion.fromPngOrThrow (png: ByteSequence) : Image {
	return graphicsPlatform.parseImageFromPngOrThrow(png)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Image.Companion.fromPng (png: ByteSequence) : Image {
	try {
		return fromPngOrThrow(png)
	} catch (_: UnableToParseException) {
		throw RuntimeException()
	}
}