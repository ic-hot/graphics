@file:Suppress("NOTHING_TO_INLINE")


package ic.graphics.image.ext


import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ByteSequence

import ic.graphics.image.Image
import ic.graphics.platform.graphicsPlatform


@Throws(UnableToParseException::class)
fun Image.Companion.fromGifOrThrowUnableToParse (gif: ByteSequence) : Image {
	return graphicsPlatform.parseImageFromGifOrThrow(gif)
}


inline fun Image.Companion.fromGif (gif: ByteSequence) : Image {
	try {
		return fromGifOrThrowUnableToParse(gif)
	} catch (t: UnableToParseException) {
		throw UnableToParseException.Runtime()
	}
}