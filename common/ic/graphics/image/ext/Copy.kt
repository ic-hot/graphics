package ic.graphics.image.ext


import ic.base.arrays.Int32Array

import ic.graphics.image.Image
import ic.graphics.image.ImageFromArgbPixelsArray


fun Image.copy() : Image {

	val width  = this.width
	val height = this.height

	val argbPixelsArray = Int32Array(length = width * height) { pixelIndex ->
		getPixelColorArgb(
			x = pixelIndex % width,
			y = pixelIndex / width
		)
	}

	return ImageFromArgbPixelsArray(
		width = width,
		height = height,
		argbPixelsArray = argbPixelsArray
	)

}