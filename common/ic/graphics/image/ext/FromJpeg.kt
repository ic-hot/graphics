package ic.graphics.image.ext


import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ByteSequence

import ic.graphics.image.Image
import ic.graphics.platform.graphicsPlatform


@Throws(UnableToParseException::class)
fun Image.Companion.fromJpegOrThrowUnableToParse (jpeg: ByteSequence) : Image {
	return graphicsPlatform.parseImageFromJpegOrThrow(jpeg)
}


@Suppress("NOTHING_TO_INLINE")
inline fun Image.Companion.fromJpeg (jpeg: ByteSequence) : Image {
	try {
		return fromJpegOrThrowUnableToParse(jpeg)
	} catch (_: UnableToParseException) {
		throw RuntimeException()
	}
}