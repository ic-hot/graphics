#ifndef ic_graphics_image_funs_SetImagePixelArgb_c_included
#define ic_graphics_image_funs_SetImagePixelArgb_c_included


#include "../../../base/primitives/int32/UInt32.h"
#include "../../../base/primitives/void/Void.h"
#include "../../../base/vectors/int32vector2/Int32Vector2.h"

#include "../../color/funs/ArgbToRgba.c"
#include "../Image.h"
#include "SetImagePixelRgba.c"


Void setImagePixelArgb (Image image, Int32Vector2 position, UInt32 argb) {

	setImagePixelRgba(
		image, position, argbToRgba(argb)
	);

}


#endif
