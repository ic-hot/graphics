#ifndef ic_graphics_image_funs_SetImagePixelColorFloat64Vector3_c_included
#define ic_graphics_image_funs_SetImagePixelColorFloat64Vector3_c_included


#include "../../../base/primitives/void/Void.h"
#include "../../../base/vectors/float64vector3/Float64Vector3.h"
#include "../../../base/vectors/int32vector2/Int32Vector2.h"

#include "../../color/funs/Float64Vector3ToRgba.c"
#include "../Image.h"
#include "SetImagePixelRgba.c"


Void setImagePixelColorFloat64Vector3 (

	Image image, Int32Vector2 position, Float64Vector3 float64Vector3

) {

	setImagePixelRgba(
		image, position, float64Vector3ToRgba(float64Vector3)
	);

}


#endif
