package ic.storage.res.impl


import platform.Foundation.NSBundle
import platform.UIKit.UIImage

import ic.base.throwables.NotExistsException

import ic.graphics.image.Image
import ic.graphics.image.ImageFromUiImage


object IosGraphicsResources : GraphicsResources {


	@Throws(NotExistsException::class)
	override fun getImageOrThrow (path: String) : Image {
		val filePath = NSBundle.mainBundle.pathForResource(
			name = "IcResources/$path",
			ofType = "png"
		)
		if (filePath == null) throw NotExistsException
		val uiImage = UIImage(contentsOfFile = filePath)
		return ImageFromUiImage(uiImage)
	}


}