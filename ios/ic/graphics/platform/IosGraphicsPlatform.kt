package ic.graphics.platform


import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.memScoped

import platform.UIKit.UIImage

import ic.base.arrays.ext.byteArrayToNsData
import ic.storage.res.impl.IosGraphicsResources
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray

import ic.graphics.image.Image
import ic.graphics.image.ImageFromUiImage


@OptIn(ExperimentalForeignApi::class, BetaInteropApi::class)
object IosGraphicsPlatform : GraphicsPlatform {


	override val resources get() = IosGraphicsResources


	override fun parseImageFromJpegOrThrow (jpeg: ByteSequence): Image {
		memScoped {
			return ImageFromUiImage(
				UIImage(
					data = byteArrayToNsData(
						jpeg.toByteArray()
					)
				)
			)
		}
	}

	override fun parseImageFromPngOrThrow (png: ByteSequence) : Image {
		memScoped {
			return ImageFromUiImage(
				UIImage(
					data = byteArrayToNsData(
						png.toByteArray()
					)
				)
			)
		}
	}

	override fun parseImageFromGifOrThrow (gif: ByteSequence): Image {
		memScoped {
			return ImageFromUiImage(
				UIImage(
					data = byteArrayToNsData(
						gif.toByteArray()
					)
				)
			)
		}
	}
	

}