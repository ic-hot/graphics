package ic.graphics.color.funs


import ic.base.annotations.UsedExternally

import ic.graphics.color.Color
import ic.graphics.color.ext.asUiColor


@UsedExternally
fun icColorToUiColor (icColor: Color) = icColor.asUiColor

@UsedExternally
fun nullableIcColorToUiColor (icColor: Color?) = icColor?.asUiColor