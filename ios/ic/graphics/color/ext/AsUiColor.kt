package ic.graphics.color.ext


import platform.UIKit.UIColor

import ic.base.primitives.float32.ext.asFloat64

import ic.graphics.color.Color


inline val Color.asUiColor get() = UIColor(
	red   = red.asFloat64,
	green = green.asFloat64,
	blue  = blue.asFloat64,
	alpha = alpha.asFloat64
)