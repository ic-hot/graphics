package ic.graphics.uikit.image.ext


import platform.Foundation.NSTemporaryDirectory
import platform.Foundation.NSURL
import platform.Foundation.writeToURL
import platform.UIKit.UIImage
import platform.UIKit.UIImagePNGRepresentation


val UIImage.asNsUrl : NSURL get() {

	val url = NSURL.fileURLWithPath(NSTemporaryDirectory() + "/${ hashCode() }.png")

	UIImagePNGRepresentation(this)!!.writeToURL(url, atomically = true)

	return url

}