package ic.graphics.image.ext


import platform.UIKit.UIImage

import ic.graphics.image.Image
import ic.graphics.image.ImageFromUiImage


inline val Image.asUiImage : UIImage get() {

	this as ImageFromUiImage

	return this.uiImage

}