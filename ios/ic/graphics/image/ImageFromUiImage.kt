package ic.graphics.image


import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.useContents

import platform.UIKit.UIImage
import platform.UIKit.UIImageJPEGRepresentation
import platform.UIKit.UIImagePNGRepresentation

import ic.base.nsdata.ext.toByteSequence
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.stream.sequence.ByteSequence

import ic.graphics.color.ColorArgb


@OptIn(ExperimentalForeignApi::class)
class ImageFromUiImage (

	val uiImage : UIImage,

	override val width  : Int64 = uiImage.size.useContents { this.width.asInt64   },
	override val height : Int64 = uiImage.size.useContents { this.height.asInt64  }

) : Image {

	override fun getPixelColorArgb(x: Int64, y: Int64): ColorArgb {
		TODO("Not yet implemented")
	}

	override fun toPng() : ByteSequence {
		return UIImagePNGRepresentation(uiImage)!!.toByteSequence()
	}

	override fun toJpeg (quality: Float32): ByteSequence {
		return UIImageJPEGRepresentation(uiImage, quality.asFloat64)!!.toByteSequence()
	}

	override fun copyResize (width: Int64, height: Int64) : Image {
		return ImageFromUiImage(
			uiImage = uiImage,
			width = width, height = height
		)
	}

}