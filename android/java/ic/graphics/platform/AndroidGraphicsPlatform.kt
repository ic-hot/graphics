package ic.graphics.platform


import android.graphics.BitmapFactory

import ic.base.arrays.ext.length
import ic.base.throwables.UnableToParseException
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray
import ic.storage.res.impl.AndroidGraphicsResources

import pl.droidsonroids.gif.GifDrawable

import ic.graphics.image.Image
import ic.graphics.image.ImageFromAndroidBitmap
import ic.graphics.image.ImageFromAndroidDrawable


internal object AndroidGraphicsPlatform : GraphicsPlatform {


	override val resources get() = AndroidGraphicsResources


	@Throws(UnableToParseException::class)
	override fun parseImageFromJpegOrThrow (jpeg: ByteSequence) : Image {
		val byteArray = jpeg.toByteArray()
		return ImageFromAndroidBitmap(
			BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length) ?: throw UnableToParseException()
		)
	}


	@Throws(UnableToParseException::class)
	override fun parseImageFromPngOrThrow (png: ByteSequence) : Image {
		val byteArray = png.toByteArray()
		return ImageFromAndroidBitmap(
			BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length) ?: throw UnableToParseException()
		)
	}


	@Throws(UnableToParseException::class)
	override fun parseImageFromGifOrThrow (gif: ByteSequence) : Image {
		return ImageFromAndroidDrawable(
			androidDrawable = GifDrawable(gif.toByteArray())
		)
	}


}