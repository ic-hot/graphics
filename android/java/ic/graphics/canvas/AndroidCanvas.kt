package ic.graphics.canvas


import android.graphics.Paint
import android.graphics.Paint.Style.FILL
import android.graphics.Rect
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32

import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.base.primitives.int64.ext.asFloat32
import ic.graphics.color.Color

import ic.graphics.color.ColorArgb
import ic.graphics.color.mix.MixMode
import ic.graphics.image.Image
import ic.graphics.image.ImageFromAndroidBitmap


abstract class AndroidCanvas : Canvas {


	protected abstract val androidCanvas : android.graphics.Canvas


	override val width  get() = androidCanvas.width.asInt64
	override val height get() = androidCanvas.height.asInt64


	private val paint = Paint().apply {
		style = FILL
	}


	override fun drawPixel (x: Int64, y: Int64, color: ColorArgb, mixMode: MixMode) {
		paint.color = color
		paint.alpha = 255
		androidCanvas.drawPoint(x.asFloat32, y.asFloat32, paint)
	}

	override fun drawPixel (x: Int64, y: Int64, color: Color, mixMode: MixMode) {
		drawPixel(
			x = x, y = y,
			color = color.argb,
			mixMode = mixMode
		)
	}


	override fun drawRect (
		x : Int64, y : Int64,
		width : Int64, height : Int64,
		color : ColorArgb,
		mixMode : MixMode
	) {
		paint.color = color
		paint.alpha = 255
		androidCanvas.drawRect(
			x.asFloat32,
			y.asFloat32,
			(x + width).asFloat32,
			(y + height).asFloat32,
			paint
		)
	}

	override fun drawRect (
		x : Int64, y : Int64,
		width : Int64, height : Int64,
		color : Color,
		mixMode : MixMode
	) {
		drawRect(
			x = x, y = y,
			width = width, height = height,
			color = color.argb,
			mixMode = mixMode
		)
	}


	private val rect = Rect()

	override fun drawImage(
		x : Int64, y : Int64,
		image : Image,
		width : Int64, height : Int64,
		opacity : Float32,
		toUseAntialiasing : Boolean,
		mixMode : MixMode
	) {
		androidCanvas.drawFilter = null
		when (image) {
			is ImageFromAndroidBitmap -> {
				paint.color = ColorArgb(0xffffffff)
				paint.isFilterBitmap = toUseAntialiasing
				paint.alpha = (opacity * 255).asInt32
				rect.left = x.asInt32
				rect.top = y.asInt32
				rect.right = (x + width).asInt32
				rect.bottom = (y + height).asInt32
				androidCanvas.drawBitmap(
					image.androidBitmap,
					null,
					rect,
					paint
				)
			}
			else -> super.drawImage(x, y, image, width, height, opacity, toUseAntialiasing, mixMode)
		}
	}


}