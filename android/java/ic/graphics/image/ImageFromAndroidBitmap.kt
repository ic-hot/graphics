package ic.graphics.image


import android.graphics.Bitmap


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.stream.sequence.ByteSequence

import ic.android.graphics.bitmap.ext.copyResize
import ic.android.graphics.bitmap.ext.toJpeg
import ic.android.graphics.bitmap.ext.toPng
import ic.graphics.color.ColorArgb


class ImageFromAndroidBitmap (

	val androidBitmap : Bitmap

) : Image {


	override val width  get() = androidBitmap.width.asInt64
	override val height get() = androidBitmap.height.asInt64


	override fun getPixelColorArgb (x: Int64, y: Int64) : ColorArgb {
		return androidBitmap.getPixel(x.asInt32, y.asInt32)
	}


	override fun toJpeg (quality: Float32) : ByteSequence {
		return androidBitmap.toJpeg(quality = quality)
	}

	override fun toPng() : ByteSequence {
		return androidBitmap.toPng()
	}


	override fun copyResize (width: Int64, height: Int64) = ImageFromAndroidBitmap(
		androidBitmap = androidBitmap.copyResize(width.asInt32, height.asInt32)
	)


}