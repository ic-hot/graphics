package ic.graphics.image.ext



import ic.graphics.image.Image
import ic.graphics.image.ImageFromAndroidBitmap

import android.graphics.Bitmap


val Image.asAndroidBitmap : Bitmap get() {

	if (this is ImageFromAndroidBitmap) {
		return androidBitmap
	}

	throw NotImplementedError()

}