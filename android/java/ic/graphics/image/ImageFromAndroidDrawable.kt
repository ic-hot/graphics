package ic.graphics.image


import android.graphics.drawable.Drawable

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64


class ImageFromAndroidDrawable (

	val androidDrawable : Drawable

) : Image {

	override val width  get() = androidDrawable.intrinsicWidth.asInt64
	override val height get() = androidDrawable.intrinsicHeight.asInt64

	override fun getPixelColorArgb (x: Int64, y: Int64) = throw NotImplementedError()

	override fun toJpeg(quality: Float32) = throw NotImplementedError()

	override fun toPng() = throw NotImplementedError()

	override fun copyResize (width: Int64, height: Int64) = throw NotImplementedError()

}