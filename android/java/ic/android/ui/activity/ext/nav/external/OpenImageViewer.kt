package ic.android.ui.activity.ext.nav.external


import android.app.Activity
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.graphics.Bitmap

import ic.android.graphics.bitmap.ext.toUriOrNull
import ic.parallel.funs.doInBackground
import ic.parallel.funs.doInUiThread
import ic.util.log.logW

import ic.graphics.image.ext.asAndroidBitmap
import ic.graphics.image.fromurl.getImageFromUrlCacheFirst


private var isImageLoading : Boolean = false

fun Activity.openImageViewer (imageUrl: String) {
	if (isImageLoading) return
	isImageLoading = true
	doInBackground {
		try {
			val image = getImageFromUrlCacheFirst(imageUrl)
			val bitmap = image.asAndroidBitmap
			doInUiThread {
				isImageLoading = false
				openImageViewer(bitmap)
			}
		} catch (e: Exception) {
			logW("Uncaught") { e }
			doInUiThread {
				isImageLoading = false
			}
		}
	}
}


fun Activity.openImageViewer (bitmap: Bitmap) {

	val intent = Intent(ACTION_VIEW, bitmap.toUriOrNull()!!)

	startActivity(intent)

}