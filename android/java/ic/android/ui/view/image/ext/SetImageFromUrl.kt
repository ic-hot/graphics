package ic.android.ui.view.image.ext


import ic.base.R
import ic.base.kfunctions.DoNothing
import ic.base.strings.ext.isBlank
import ic.base.throwables.ext.stackTraceAsString
import ic.design.task.Task
import ic.design.task.scope.ext.doInUiThread
import ic.parallel.funs.doInBackgroundAsTask
import ic.struct.map.ephemeral.EphemeralKeysMap
import ic.struct.value.ephemeral.Weak
import ic.util.log.logW

import ic.graphics.util.ResizeMode
import ic.graphics.image.fromurl.getImageFromUrlCacheFirst

import android.widget.ImageView


internal val alreadySetImageUrls = EphemeralKeysMap<ImageView, String>(::Weak)


fun ImageView.setImageFromUrl (

	urlString : String?,

	resizeMode : ResizeMode = ResizeMode.LeaveAsIs,

	onFinish : () -> Unit = DoNothing,

) : Task? {

	if (urlString != null) {
		if (alreadySetImageUrls[this] == urlString) {
			onFinish()
			return null
		}
		alreadySetImageUrls[this] = urlString
	}

	setImageResource(R.color.transparent)

	if (urlString == null || urlString.isBlank) {
		onFinish()
		return null
	}

	return doInBackgroundAsTask {
		try {
			val image = getImageFromUrlCacheFirst(
				urlString = urlString,
				resizeMode = resizeMode
			)
			doInUiThread {
				onFinish()
				setImage(image)
			}
		} catch (e: Exception) {
			logW("Uncaught") { "ImageView.setImageFromUrl " + e.stackTraceAsString }
		}
	}

}