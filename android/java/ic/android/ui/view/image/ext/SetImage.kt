@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.image.ext


import android.graphics.drawable.Animatable
import android.widget.ImageView

import ic.graphics.image.Image
import ic.graphics.image.ImageFromAndroidBitmap
import ic.graphics.image.ImageFromAndroidDrawable


inline fun ImageView.setImage (image : Image?) {

	when (image) {

		null -> clearImage()

		is ImageFromAndroidBitmap -> setImageBitmap(image.androidBitmap)

		is ImageFromAndroidDrawable -> {
			val drawable = image.androidDrawable
			setImageDrawable(drawable)
			if (drawable is Animatable) {
				drawable.start()
			}
		}

	}

}