package ic.android.ui.view.image.ext


import android.widget.ImageView

import ic.base.R


fun ImageView.clearImage() {

	alreadySetImageUrls[this] = null

	setImageResource(R.color.transparent)

}