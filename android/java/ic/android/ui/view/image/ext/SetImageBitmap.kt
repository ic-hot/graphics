package ic.android.ui.view.image.ext


import ic.base.R

import android.graphics.Bitmap
import android.widget.ImageView


fun ImageView.setImage (bitmap : Bitmap?) {
	if (bitmap == null) {
		setImageResource(R.color.transparent)
	} else {
		setImageBitmap(bitmap)
	}
}