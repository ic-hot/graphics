@file:Suppress("DEPRECATION")


package ic.android.graphics.bitmap.ext.blur


import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.ifaces.cancelable.Cancelable
import ic.parallel.funs.doInBackground

import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur

import ic.android.system.isHuawei
import ic.android.system.isXiaomi
import ic.android.util.handler.post
import ic.android.app.thisApp
import ic.android.graphics.bitmap.ext.copyResize


private val maxSupportedBlurRadius : Int32 = when {
	isXiaomi -> 4
	isHuawei -> 4
	else -> 8
}


fun Bitmap.copyBlur (radius: Int32) : Bitmap {

	if (radius < 0.5) return this

	if (maxSupportedBlurRadius == 0) return copyBlurByDownscaling(radius)

	val downscaledBitmap = if (radius <= maxSupportedBlurRadius) {
		this
	} else {
		this.copyResize(
			this.width * maxSupportedBlurRadius / radius,
			this.height * maxSupportedBlurRadius / radius,
		)
	}

	val renderScript = RenderScript.create(thisApp)
	val input = Allocation.createFromBitmap(renderScript, downscaledBitmap)
	val blurScript = ScriptIntrinsicBlur.create(renderScript, input.element)
	val output = Allocation.createTyped(renderScript, input.type)
	blurScript.setRadius(
		(
			if (radius <= maxSupportedBlurRadius) radius else maxSupportedBlurRadius
		).asFloat32
	)
	blurScript.setInput(input)
	blurScript.forEach(output)
	input.destroy()
	output.copyTo(downscaledBitmap)
	output.destroy()

	val upscaledBitmap = if (downscaledBitmap === this) {
		this
	} else {
		downscaledBitmap.copyResize(this.width, this.height).also {
			downscaledBitmap.recycle()
		}
	}

	return upscaledBitmap

}


inline fun Bitmap.copyBlur (
	radius: Int32, crossinline onSuccess: (Bitmap) -> Unit
) : Cancelable? {

	if (radius < 0.5) {
		onSuccess(this)
		return null
	}

	var isCanceled : Boolean = false

	doInBackground {
		val blurResult = copyBlur(radius)
		post {
			if (!isCanceled) {
				onSuccess(blurResult)
			}
		}
	}

	return object : Cancelable {
		override fun cancel() {
			isCanceled = true
		}
	}

}