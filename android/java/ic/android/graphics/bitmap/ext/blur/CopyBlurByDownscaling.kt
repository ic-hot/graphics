package ic.android.graphics.bitmap.ext.blur


import android.graphics.Bitmap

import ic.android.graphics.bitmap.ext.copyResize

import ic.base.primitives.int32.Int32


internal fun Bitmap.copyBlurByDownscaling (radius: Int32) : Bitmap {

	if (radius < 2) return this

	val width = width
	val height = height

	val downscaledBitmap = copyResize(width / radius, height / radius)

	val upscaledBitmap = downscaledBitmap.copyResize(width, height)

	return upscaledBitmap

}