package ic.android.graphics.bitmap.ext


import android.graphics.Bitmap

import ic.graphics.image.ImageFromAndroidBitmap


inline val Bitmap.asImage get() = ImageFromAndroidBitmap(this)