package ic.android.graphics.bitmap.ext


import android.content.ContentValues
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore

import ic.android.app.thisApp



fun Bitmap.toUriOrNull (

	title : String? = null,

	description : String? = null

) : Uri? {

	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

		val contentValues = ContentValues()
		contentValues.put(MediaStore.Images.Media.TITLE, title)
		contentValues.put(MediaStore.Images.Media.DESCRIPTION, description)

		val resolver = thisApp.contentResolver
		val imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

		if (imageUri != null) {
			val fos = resolver.openOutputStream(imageUri)!!
			this.compress(Bitmap.CompressFormat.PNG, 100, fos)
			fos.flush()
			fos.close()
		}

		contentValues.clear()

		if (imageUri != null) {
			resolver.update(imageUri, contentValues, null, null)
		}

		return imageUri

	} else {

		@Suppress("DEPRECATION")
		val uriString = MediaStore.Images.Media.insertImage(
			thisApp.contentResolver,
			this,
			title,
			description
		)

		if (uriString == null) return null

		return Uri.parse(uriString)

	}


}