package ic.android.graphics.bitmap.ext


import java.io.ByteArrayOutputStream

import android.graphics.Bitmap

import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.stream.sequence.ByteSequence


fun Bitmap.toJpegByteArray (quality: Float32 = .75F) : ByteArray {
	val stream = ByteArrayOutputStream()
	compress(
		Bitmap.CompressFormat.JPEG,
		(quality * 100).asInt32,
		stream
	)
	return stream.toByteArray()
}

fun Bitmap.toJpeg (quality: Float32 = .75F) = ByteSequence(toJpegByteArray(quality = quality))