package ic.android.graphics.bitmap.ext


import java.io.ByteArrayOutputStream

import android.graphics.Bitmap

import ic.stream.sequence.ByteSequence


fun Bitmap.toPngByteArray() : ByteArray {
	val stream = ByteArrayOutputStream()
	compress(Bitmap.CompressFormat.PNG, 100, stream)
	return stream.toByteArray()
}

fun Bitmap.toPng() = ByteSequence(toPngByteArray())