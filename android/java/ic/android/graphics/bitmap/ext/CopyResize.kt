package ic.android.graphics.bitmap.ext


import android.graphics.Bitmap


fun Bitmap.copyResize (newWidth: Int, newHeight: Int, toAntialias: Boolean = true) : Bitmap {
	return Bitmap.createScaledBitmap(this, newWidth, newHeight, toAntialias)
}
