package ic.android.graphics.bitmap.ext


import android.graphics.Bitmap

import ic.base.primitives.int32.ext.asFloat32


inline val Bitmap.aspectRatio get() = width.asFloat32 / height