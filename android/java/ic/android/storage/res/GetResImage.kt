package ic.android.storage.res


import ic.android.graphics.bitmap.ext.asImage
import ic.base.primitives.int32.Int32
import ic.graphics.image.Image
import ic.util.cache.Cache
import ic.util.cache.RamCache


private val imagesByResId : Cache<Int32, Image> = RamCache()


fun getResImage (resId: Int32) : Image {

	return imagesByResId.getOr(resId) { getResBitmap(resId).asImage }

}