package ic.android.storage.res


import ic.android.app.thisApp
import ic.base.primitives.int32.Int32
import ic.util.cache.RamCache

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb


fun getResColorArgb (resId : Int32) : ColorArgb {
	@Suppress("DEPRECATION")
	return thisApp.resources.getColor(resId)
}


private val colorsCache = RamCache<Int32, Color>()

fun getResColor (resId : Int32) : Color {
	return colorsCache.getOr(resId) {
		Color(getResColorArgb(resId))
	}
}