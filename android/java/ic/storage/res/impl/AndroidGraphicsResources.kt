package ic.storage.res.impl


import ic.android.storage.assets.getAssetBitmapOrNull
import ic.android.storage.res.getResBitmapOrNull
import ic.base.throwables.NotExistsException

import ic.graphics.image.Image
import ic.graphics.image.ImageFromAndroidBitmap


internal object AndroidGraphicsResources : GraphicsResources {

	@Throws(NotExistsException::class)
	override fun getImageOrThrow (path: String) : Image {
		val bitmap = (
			getAssetBitmapOrNull("$path.png") ?:
			getResBitmapOrNull(path)
		)
		if (bitmap == null) throw NotExistsException
		return ImageFromAndroidBitmap(bitmap)
	}

}