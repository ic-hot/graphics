package ic.graphics.image.show.awt


import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JLabel

import ic.graphics.image.Image
import ic.graphics.image.ext.toBufferedImage


class ImageWindow

	internal constructor (
		private val jFrame: JFrame
	)

{

	private var jLabel : JLabel? = null

	fun setImage (image: Image) {

		if (jLabel != null) {
			jFrame.remove(jLabel)
		}

		val bufferedImage = image.toBufferedImage()

		jLabel = JLabel(ImageIcon(bufferedImage))

		jFrame.add(jLabel)

		jFrame.pack()

	}

}