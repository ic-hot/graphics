package ic.graphics.image.show.awt


import java.awt.Dimension
import java.awt.Toolkit
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import javax.swing.JFrame

import ic.graphics.image.Image


fun showImageInAwtWindow (image: Image) : ImageWindow {

	val jFrame = JFrame()

	val screenSize : Dimension = Toolkit.getDefaultToolkit().screenSize
	jFrame.setLocation(screenSize.width / 2 - jFrame.size.width / 2, screenSize.height / 2 - jFrame.size.height / 2)

	val imageWindow = ImageWindow(jFrame)

	imageWindow.setImage(image)

	jFrame.addKeyListener(object : KeyListener {
		override fun keyTyped (keyEvent: KeyEvent) {
			jFrame.isVisible = false
			jFrame.dispose()
		}
		override fun keyPressed (keyEvent: KeyEvent) {}
		override fun keyReleased (keyEvent: KeyEvent) {}
	})

	jFrame.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
	jFrame.isVisible = true

	return imageWindow

}