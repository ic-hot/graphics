package ic.graphics.image.ext


import ic.stream.sequence.ByteSequence

import ic.graphics.image.Image
import ic.stream.buffer.ByteBuffer
import ic.stream.output.ext.asOutputStream
import javax.imageio.ImageIO


fun Image.toJpg() : ByteSequence {

	val bufferedImage = toBufferedImage()
	val byteBuffer = ByteBuffer()
	ImageIO.write(bufferedImage, "jpg", byteBuffer.asOutputStream)
	return byteBuffer

}