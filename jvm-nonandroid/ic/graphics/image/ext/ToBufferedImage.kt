@file:Suppress("NOTHING_TO_INLINE")


package ic.graphics.image.ext


import java.awt.image.BufferedImage

import ic.base.loop.breakableRepeat
import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.ext.asInt32

import ic.graphics.image.Image


fun Image.toBufferedImage() : BufferedImage {

	val width  = width.asInt32
	val height = height.asInt32

	val bufferedImage = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)

	breakableRepeat(height) { y ->
		breakableRepeat(width) { x ->

			bufferedImage.setRGB(
				x, y,
				getPixelColorArgb(x.asInt64, y.asInt64)
			)

		}
	}

	return bufferedImage

}