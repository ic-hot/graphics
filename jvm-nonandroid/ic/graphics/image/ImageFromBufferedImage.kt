package ic.graphics.image


import java.awt.image.BufferedImage
import javax.imageio.ImageIO

import ic.base.primitives.int32.ext.asInt64
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asInt32
import ic.stream.buffer.ByteBuffer
import ic.stream.output.ext.asOutputStream
import ic.stream.sequence.ByteSequence

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb
import ic.graphics.color.ext.fromArgb
import ic.graphics.image.ext.toBufferedImage


class ImageFromBufferedImage (

	private val bufferedImage : BufferedImage

) : Image {

	override val width  get() = bufferedImage.width.asInt64
	override val height get() = bufferedImage.height.asInt64

	override fun getPixelColorArgb (x: Int64, y: Int64): ColorArgb {
		return bufferedImage.getRGB(x.asInt32, y.asInt32)
	}

	override fun getPixelColor (x: Int64, y: Int64) : Color {
		return Color.fromArgb(
			getPixelColorArgb(x, y)
		)
	}

	override fun toJpeg() : ByteSequence {
		val bufferedImage = toBufferedImage()
		val byteBuffer = ByteBuffer()
		ImageIO.write(bufferedImage, "jpeg", byteBuffer.asOutputStream)
		return byteBuffer
	}

	override fun toPng() : ByteSequence {
		val bufferedImage = toBufferedImage()
		val byteBuffer = ByteBuffer()
		ImageIO.write(bufferedImage, "png", byteBuffer.asOutputStream)
		return byteBuffer
	}

	override fun copyResize (width: Int64, height: Int64): Image {
		TODO("Not yet implemented")
	}

}