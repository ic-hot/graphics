package ic.graphics.platform


import ic.stream.sequence.ByteSequence

import ic.graphics.image.Image
import ic.storage.res.GraphicsResources


internal object NonAndroidJvmGraphicsPlatform : GraphicsPlatform {

	override val resources: GraphicsResources
		get() = TODO("Not yet implemented")

	override fun parseImageFromJpegOrThrowUnableToParse(jpeg: ByteSequence): Image {
		TODO("Not yet implemented")
	}

	override fun parseImageFromPngOrThrowUnableToParse(png: ByteSequence): Image {
		TODO("Not yet implemented")
	}

	override fun parseImageFromGifOrThrowUnableToParse(gif: ByteSequence): Image {
		TODO("Not yet implemented")
	}

}